//
//  FactorsSort.h
//  Music Clustering
//
//  Created by Sukhanov Nickolay on 12/14/13.
//  Copyright (c) 2013 Sukhanov Nickolay. All rights reserved.
//

#ifndef __Music_Clustering__FactorsSort__
#define __Music_Clustering__FactorsSort__

#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include "Song.h"

const static double FACTOR = 0.27;

typedef size_t AuthorId;

struct Edge {
    size_t vertexTo;
    size_t weight;
};

struct Vertex {
    size_t visited;
    std::vector<Edge> edges;
};

class FactorSort {
public:
    void buildGraph();
    FactorSort(const std::string &fileName);
    Song& getNext() const;
    size_t getDifferent() const;
    size_t getSize() const;
    void resetCounter();
    void clustering(std::string out);
    const std::vector<Song> &getSongs() const;
private:
    void dfs(size_t start, std::ostream &out);
    size_t different_;
    std::unordered_map< AuthorId, std::vector<size_t> > ids_;
    std::unordered_map< std::string, std::unordered_set<AuthorId> > compressedIds_;
    std::vector<size_t> coordinatesCompressing_;
    std::vector<size_t> compressedIdToSongNumber_;
    std::vector<Song> songs_;
    size_t currentSong_;
    std::ifstream inputFile_;
    std::vector<Vertex> adjacencyLists_;

};

#endif /* defined(__Music_Clustering__FactorsSort__) */
