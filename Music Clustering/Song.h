//
//  Song.h
//  Music Clustering
//
//  Created by Sukhanov Nickolay on 12/14/13.
//  Copyright (c) 2013 Sukhanov Nickolay. All rights reserved.
//

#ifndef __Music_Clustering__Song__
#define __Music_Clustering__Song__

#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <unordered_map>

const static size_t ASCII_SIZE = 128;

class Song {
public:
    Song();
    Song(const Song &song);
    size_t getId() const;
    double distance(const Song &second) const;
    double levenshteinDistance(const Song &second) const;
    size_t projectedFactor() const;
    void printAuthor(std::ostream &out);
    std::string &printAuthor();
    std::string normalize(std::string &input);
    static std::unordered_map<std::string, double> authorWordFreq;
    static std::unordered_map<std::string, double> songWordFreq;
private:
    
    std::istream &read(std::istream &input);
    std::ostream &output(std::ostream &output);
    size_t id_;
    std::vector<std::string> singer_;
    std::string singerString_;
    std::vector<std::string> title_;
    friend std::istream &operator>>(std::istream  &input, Song &s);
    friend std::ostream &operator<<(std::ostream  &output, Song &s);
};

#endif /* defined(__Music_Clustering__Song__) */
