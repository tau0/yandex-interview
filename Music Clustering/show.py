import sys

ftr = open('../tracks.txt');
fin = open(sys.argv[1]);
fout = open(sys.argv[2], 'w');

tracks = dict()
for line in ftr:
    tracks[line.split()[0]] = line

for line in fin:
    words = [a for a in line.split() if a]
    if len(words) >= 2:
        for word in words:
            print >>fout, tracks[word]
        print >>fout, '############################################'

