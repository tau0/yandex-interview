//
//  FactorsSort.cpp
//  Music Clustering
//
//  Created by Sukhanov Nickolay on 12/14/13.
//  Copyright (c) 2013 Sukhanov Nickolay. All rights reserved.
//

#include "FactorsSort.h"


FactorSort::FactorSort(const std::string &fileName): currentSong_(0), different_(0) {
    inputFile_.open(fileName);
    if (!inputFile_) {
        throw std::runtime_error("..");
    }
    Song current;
    size_t counter = 0;
    while(!inputFile_.eof() && !inputFile_.bad() && inputFile_ >> current) {
        counter++;
        if (ids_.find(current.getId()) == ids_.end()) {
            ++different_;
            ids_.insert(std::make_pair(current.getId(), std::vector<size_t>()));
            ids_[current.getId()].push_back(songs_.size());
        } else {
            ids_[current.getId()].push_back(songs_.size());
        }
        if (compressedIds_.find(current.printAuthor()) == compressedIds_.end()) {
            compressedIds_.insert(std::make_pair(current.printAuthor(), std::unordered_set<AuthorId>()));
            compressedIds_[current.printAuthor()].insert(current.getId());
        } else {
            compressedIds_[current.printAuthor()].insert(current.getId());
        }
        songs_.push_back(current);
//        if (counter >= 1000 || current.getId() == 29312) {
//            std::cout << current;
//            break;
//        }
    }
    
    double numberOfWords = 0;
    for (auto i = Song::authorWordFreq.begin(); i != Song::authorWordFreq.end(); ++i) {
        i->second = 1 / i->second;
        numberOfWords += i->second;
    }
    
    for (auto i = Song::authorWordFreq.begin(); i != Song::authorWordFreq.end(); ++i) {
        i->second /= numberOfWords;
    }
    
    numberOfWords = 0;
    for (auto i = Song::songWordFreq.begin(); i != Song::songWordFreq.end(); ++i) {
        i->second = 1 / i->second;
        numberOfWords += i->second;
    }
    for (auto i = Song::songWordFreq.begin(); i != Song::songWordFreq.end(); ++i) {
        i->second /= numberOfWords;
    }
    std::cout << "Readed: " << getSize() << ", Different: " << getDifferent() << " diff: " << compressedIds_.size()
    << " " << Song::authorWordFreq.size() << std::endl;
}

size_t FactorSort::getDifferent() const {
    return different_;
}

size_t FactorSort::getSize() const {
    return songs_.size();
}

const std::vector<Song> &FactorSort::getSongs() const {
    return songs_;
}

void FactorSort::buildGraph() {
    size_t counter = 0;
    for (auto i = compressedIds_.begin(); i != compressedIds_.end(); ++i) {
        coordinatesCompressing_.push_back(ids_[*((i->second).begin())].front());
    }
    for (auto i = 0; i != coordinatesCompressing_.size(); ++i) {
        Vertex vertex;
        vertex.visited = 0;
        for (auto j = 0; j != coordinatesCompressing_.size(); ++j) {
            counter++;
            if(counter % 1000000 == 0) {
                std::cout << counter << std::endl;
            }
            auto& from = songs_[coordinatesCompressing_[i]];
            auto& to = songs_[coordinatesCompressing_[j]];
            auto distance = from.distance(to);
            if (distance < FACTOR) {
                Edge edge;
                edge.vertexTo = j;
                edge.weight = distance;
                vertex.edges.push_back(edge);
            }
        }
        adjacencyLists_.push_back(vertex);
    }
}

void FactorSort::dfs(size_t start, std::ostream &out) {
    adjacencyLists_[start].visited = true;
//    songs_[coordinatesCompressing_[start]].printAuthor(out);
//    out << "|" << songs_[coordinatesCompressing_[start]].printAuthor();
    auto begin = compressedIds_[songs_[coordinatesCompressing_[start]].printAuthor()].begin();
    auto end = compressedIds_[songs_[coordinatesCompressing_[start]].printAuthor()].end();
    while (begin != end) {
        out << *begin++ << "\t";
    }
    for (size_t i = 0; i < adjacencyLists_[start].edges.size(); ++i) {
        if (!adjacencyLists_[adjacencyLists_[start].edges[i].vertexTo].visited) {
            dfs(adjacencyLists_[start].edges[i].vertexTo, out);
        }
    }
    
}

void FactorSort::clustering(std::string outFileName) {
    std::ofstream out;
    out.open(outFileName);
    
    if (!out) {
        throw std::runtime_error("..");
    }
    
    buildGraph();
    
    for (size_t i = 0; i < coordinatesCompressing_.size(); ++i) {
        if (!adjacencyLists_[i].visited) {
            dfs(i, out);
            out << "\n";
        }
    }
}