fin = open('answer.txt')
fsample = open('sample_clusters.txt')
fdiff = open('diff.txt', 'w')

ids = dict()

for line in fin:
    words = line.split()
    words.sort()
    words = [a for a in words if a]
    ids[words[0]] = words


broken = 0
clusters = 0
valid = 0
for line in fsample:
    words = line.split()
    words.sort()
    words = [a for a in words if a]
    if words[0] in ids and len(ids[words[0]]) == len(words):
        flag = 0
        for i in range(0, len(words)):
            if ids[words[0]][i] != words[i]:
                print >>fdiff, ' '.join(words)
                broken += 1
                break
                flag = 1
        if not flag and len(words) >= 2:
            valid += 1
    else:
        print >>fdiff, ' '.join(words)
        broken += 1

    clusters += 1
print broken
print valid
print clusters
print 100 - broken * 100. / clusters
