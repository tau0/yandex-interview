//
//  main.cpp
//  Music Clustering
//
//  Created by Sukhanov Nickolay on 12/14/13.
//  Copyright (c) 2013 Sukhanov Nickolay. All rights reserved.
//

#include <iostream>
#include "FactorsSort.h"

const static std::string INPUT_FILE = "./output.txt";
const static std::string OUTPUT_FILE = "./answer.txt";

int main(int argc, const char * argv[])
{
    std::ios_base::sync_with_stdio(false);

    FactorSort factor(INPUT_FILE);
    
    factor.clustering(OUTPUT_FILE);
    return 0;
}

