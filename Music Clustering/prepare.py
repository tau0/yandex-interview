ftr = open("../tracks.txt")
fin = open('../sample_clusters.txt')
fout = open('tracks_cut.txt', 'w')


ids = dict()
for line in ftr:
    ids[line.split()[0]] = line;

answer = ''
for line in fin:
    for word in line.split():
        answer += ids[word]

print >>fout, answer
