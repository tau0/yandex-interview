//
//  Song.cpp
//  Music Clustering
//
//  Created by Sukhanov Nickolay on 12/14/13.
//  Copyright (c) 2013 Sukhanov Nickolay. All rights reserved.
//

#include "Song.h"



template<class T>
unsigned int edit_distance(const T &s1, const T & s2) {
	const size_t len1 = s1.size(), len2 = s2.size();
    std::vector<unsigned int> col(len2+1), prevCol(len2+1);
    
	for (unsigned int i = 0; i < prevCol.size(); i++)
		prevCol[i] = i;
	for (unsigned int i = 0; i < len1; i++) {
		col[0] = i+1;
		for (unsigned int j = 0; j < len2; j++)
			col[j+1] = std::min(std::min( 1 + col[j], 1 + prevCol[1 + j]),
                           prevCol[j] + (s1[i]==s2[j] ? 0 : 1) );
		col.swap(prevCol);
	}
	return prevCol[len2];
}

Song::Song(): id_(0) {}
auto Song::authorWordFreq = std::unordered_map<std::string, double>();
auto Song::songWordFreq = std::unordered_map<std::string, double>();

Song::Song(const Song &song): id_(song.id_) {
    singer_ = song.singer_;
    title_ = song.title_;
    singerString_ = song.singerString_;
    
}

std::string Song::normalize(std::string &input) {
    std::string answer = "";
    for (size_t i = 0; i < input.size(); ++i) {
        if ((input[i] >= 'A' && input[i] <= 'Z')) {
            answer.push_back(input[i] - 'A' + 'a');
            continue;
        }
        answer.push_back(input[i]);
    }
    return answer;
}

std::istream &Song::read(std::istream &input) {
    id_ = 0;
    singer_.clear();
    title_.clear();
    singerString_.clear();
    input >> id_;
    size_t counter = 0;
    
    input >> counter;

    for (size_t i = 0; i < counter; ++i) {
        std::string buffer = "";
        input >> buffer;
        buffer = normalize(buffer);
        if(buffer.empty()) {
            continue;
        }
        singer_.push_back(buffer);
    }
    
    sort(singer_.begin(), singer_.end());
    for (size_t i = 0; i < singer_.size(); ++i) {
        authorWordFreq[singer_[i]] += 1;
        singerString_ += singer_[i] + ' ';
    }
    
    input >> counter;
    for (size_t i = 0; i < counter; ++i) {
        std::string buffer = "";
        input >> buffer;
        buffer = normalize(buffer);
        if(buffer.empty()) {
            continue;
        }
        title_.push_back(buffer);
    }
    sort(title_.begin(), title_.end());
    for (size_t i = 0; i < title_.size(); ++i) {
        songWordFreq[title_[i]] += 1;
        
    }
    return input;
}

std::ostream &Song::output(std::ostream &output) {
    output << id_ << "\t";
    for (size_t i = 0; i < singer_.size(); ++i) {
        output << singer_[i] << " ";
    }
    
    output << "\t";
    for (size_t i = 0; i < title_.size(); ++i) {
        output << title_[i] << " ";
    }
    
    
    return output;
}



size_t Song::getId() const {
    return id_;
}

std::string &Song::printAuthor() {
    return singerString_;
}

void Song::printAuthor(std::ostream &out) {
    for (size_t i = 0; i < singer_.size(); ++i) {
        out << singer_[i] << " ";
    }
    out << "\t";
}

int static const MAX_VALUE = 1;
int static const BIG_DISTANCE = 100;

double Song::levenshteinDistance(const Song &second) const {
    std::string firstSinger = "";
    for (size_t i = 0; i < singer_.size(); ++i) {
        firstSinger += singer_[i];
    }
    std::string secondSinger = "";
    for (size_t i = 0; i < second.singer_.size(); ++i) {
        secondSinger += second.singer_[i];
    }
    double dist = edit_distance(secondSinger, firstSinger);
    double size = std::max(secondSinger.size(), firstSinger.size());
    return dist / size;
}

double Song::distance(const Song &second) const {
    if ((getId() == 16523 && second.getId() == 7751) || (getId() == 7751 && second.getId() == 16523)) {
        int a = getId();
    }
    size_t i = 0;
    size_t j = 0;
    double answer = 0;
    double firstWeight = 0;
    double secondWeight = 0;
    
    for (size_t i = 0; i < singer_.size(); ++i) {
        firstWeight += Song::authorWordFreq[singer_[i]];
    }
    for (size_t i = 0; i < second.singer_.size(); ++i) {
        secondWeight += Song::authorWordFreq[second.singer_[i]];
    }
    
    size_t counter = 0;
    while (i < singer_.size() && j < second.singer_.size()) {
        if (singer_[i] < second.singer_[j]) {
            ++i;
        } else if (singer_[i] > second.singer_[j]) {
            ++j;
        } else {
            counter++;
            answer += Song::authorWordFreq[singer_[i]];
            ++i;
            ++j;
        }
    }
    if(counter == 1 && std::min(singer_.size(), second.singer_.size()) == 2) {
        answer = 0;
    }

    
    double value = 1 - (2 * answer) / (firstWeight + secondWeight);
    
    if (abs(getId() - second.getId()) > BIG_DISTANCE)
        return MAX_VALUE;
    return std::min(value, levenshteinDistance(second));
}

size_t Song::projectedFactor() const {
    return singer_.size();
}

std::istream &operator>>(std::istream  &input, Song &s) {
    return s.read(input);
}

std::ostream &operator<<(std::ostream  &output, Song &s) {
    return s.output(output);
}