from __future__ import print_function

tracks = open("tracks_cut.txt")
output = open("output.txt", "w")

forbidden = ',\'.\"!@#$%^&*()/?'

for line in tracks:
    songIsGood = True
    ID, author, song = line.split("\t")
    ID = ID.strip()
    author = author.strip()
    song = song.strip()

    if author:
        author = author.lower()
        ans = []
        for a in author:
            if not (a in forbidden):
                ans.append(a)
        author = ''.join(ans)
        if not author:
            songIsGood  = False
        else:
            author = [a for a in author.strip().split() if a]
    else:
        songIsGood = False

    if song:
        song = song.lower()
        song = [a for a in song.strip().split() if a]
    else:
        songIsGood = False

    if not songIsGood:
        continue

    print(ID, file=output)
    if author:
        print(len(author), file=output)
        for word in author:
            print(word, file=output)
    else:
        print(0, file=output)

    if song:
        print(len(song), file=output)
        for word in song:
            print(word, file=output)
    else:
        print(0, file=output)

